import datetime

import json
import pytest
import pydantic

from orgreg_client.models import (
    OrgUnit,
    OrgUnitList,
    Endpoints,
    EndpointsConfig,
    ClientConfig,
    BaseModel,
)

from orgreg_client.models import _from_lower_camel  # noqa


def test_from_lower_camel():
    values = {
        "aa": "aa",
        "aa_bb_cc_dd": "aaBbCcDd",
    }
    for a, b in values.items():
        assert _from_lower_camel(a) == b


def _check_field(a: BaseModel, b: dict, field: str, alias=lambda x: x):
    value_a = a[field]
    value_b = b.get(alias(field))
    if isinstance(value_a, BaseModel):
        for f in value_a.__fields__:
            _check_field(value_a, value_b, f, alias)
    else:
        if isinstance(value_a, datetime.date):
            value_b = pydantic.parse_obj_as(type(value_a), value_b)
        if isinstance(value_a, list) and value_b is None:
            value_b = []

        assert value_a == value_b


def test_from_dict(config, ou_list_data):
    c = ClientConfig.from_dict(config)
    for field in c.__fields__:
        assert c[field] == config[field]

    ou_data = ou_list_data[0]
    ou = OrgUnit.from_dict(ou_data)

    for f in ou.__fields__:
        _check_field(ou, ou_data, f, _from_lower_camel)


def test_from_json(config, ou_list_data):
    c = ClientConfig.from_json(json.dumps(config))
    for field in c.__fields__:
        assert c[field] == config[field]

    ou_data = ou_list_data[0]
    ou = OrgUnit.from_json(json.dumps(ou_data))

    for f in ou.__fields__:
        _check_field(ou, ou_data, f, _from_lower_camel)


def test_endpoints():
    e = Endpoints(
        EndpointsConfig.from_dict(
            {"base_url": "https://example.com/orgreg/v3/", "ou": "ou"}
        )
    )
    assert str(e.ou) == "https://example.com/orgreg/v3/ou"

    e = Endpoints(
        EndpointsConfig.from_dict(
            {"base_url": "https://example.com/orgreg/v3/", "ou": "/ou"}
        )
    )
    assert str(e.ou) == "https://example.com/ou"

    e = Endpoints(
        EndpointsConfig.from_dict(
            {"base_url": "https://example.com/orgreg/v3/", "ou": "/ou/"}
        )
    )
    assert str(e.ou) == "https://example.com/ou/"

    e = Endpoints(
        EndpointsConfig.from_dict(
            {
                "base_url": "https://example.com/orgreg/v3/",
                "ou": "https://test.example.com/ou",
            }
        )
    )
    assert str(e.ou) == "https://test.example.com/ou"


def test_endpoints_no_extra_fields():
    with pytest.raises(pydantic.ValidationError):
        EndpointsConfig.from_dict(
            {"base_url": "https://example.com/orgreg/v3/", "|#1": "asd"}
        )


def test_ou_list(ou_list_data):
    xs = OrgUnitList.from_json(ou_list_data)
    for ou in xs.__root__:
        assert isinstance(ou, OrgUnit)


def test_org_unit(single_ou):
    class Ou(OrgUnit):
        class Config:
            extra = pydantic.Extra.forbid

    # noinspection Pydantic
    ou = Ou(**single_ou)
    assert json.loads(ou.json(by_alias=True)) == single_ou
