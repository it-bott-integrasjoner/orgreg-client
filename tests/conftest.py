import json
import os

import pytest

from orgreg_client.client import get_client


def load_json_file(name):
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(here, "fixtures", name)) as f:
        data = json.load(f)
    return data


@pytest.fixture
def base_url():
    return "https://example.com/"


@pytest.fixture
def config(base_url):
    return load_json_file("../../example-config.json")


@pytest.fixture
def client(config):
    return get_client(**config)


@pytest.fixture
def ou_list_data():
    return load_json_file("ou-list.json")


@pytest.fixture
def ou_by_sap_id_data():
    return load_json_file("ou_by_sap_id.json")


@pytest.fixture
def ou_by_id():
    return load_json_file("ou-list-w-eph-id.json")


@pytest.fixture
def ou_by_id_not_populate():
    return load_json_file("out-list-populate-eq-false.json")


@pytest.fixture
def single_ou():
    return load_json_file("ou.json")
