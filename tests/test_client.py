from orgreg_client.client import Client
from orgreg_client.models import OrgUnit


def test_get_dist_lists(client: Client, requests_mock, ou_list_data):
    requests_mock.get(
        "https://example.com/orgreg/v3/ou/",
        json=ou_list_data,
    )
    received = client.get_ou()
    expected = ou_list_data
    assert len(expected) == len(received)
    for ou in received:
        assert isinstance(ou, OrgUnit)


def test_get_ou_by_sap_id(client: Client, requests_mock, ou_by_sap_id_data):
    requests_mock.get(
        "https://example.com/orgreg/v3/ou/search/dfo_org_id/10000193?populate=true",
        json=ou_by_sap_id_data,
    )
    received = client.get_ou_by_sap_id(10000193)
    expected = ou_by_sap_id_data["ous"]
    assert len(expected) == len(received)
    for ou in received:
        assert isinstance(ou, OrgUnit)


def test_get_ou_by_ext_key_type(client: Client, requests_mock, ou_by_sap_id_data):
    requests_mock.get(
        "https://example.com/orgreg/v3/ou/search/x/y?populate=true",
        json=ou_by_sap_id_data,
    )
    received = client.get_ou_by_ext_key("x", "y")
    expected = ou_by_sap_id_data["ous"]
    assert len(expected) == len(received)
    for ou in received:
        assert isinstance(ou, OrgUnit)


def test_get_ou_by_ext_key_not_populate(
    client: Client, requests_mock, ou_by_id_not_populate
):
    requests_mock.get(
        "https://example.com/orgreg/v3/ou/search/x/y",
        json=ou_by_id_not_populate,
    )
    ext_key = "x"
    ext_value = "y"

    ext_key_value = client.get_ou_by_ext_key(ext_key, ext_value, False)
    assert ext_key_value is not None
    assert len(ext_key_value) == 2


def test_get_ou_by_ext_key_type_no_key_value(
    client: Client, requests_mock, ou_by_sap_id_data
):
    requests_mock.get(
        "https://example.com/orgreg/v3/ou/search/x/?populate=true",
        json=ou_by_sap_id_data,
    )
    received = client.get_ou_by_ext_key("x")
    expected = ou_by_sap_id_data["ous"]
    assert len(expected) == len(received)
    for ou in received:
        assert isinstance(ou, OrgUnit)


def test_get_ou_w_ou_id(client: Client, requests_mock, ou_by_id):
    requests_mock.get(
        "https://example.com/orgreg/v3/ou/1",
        json=ou_by_id,
    )
    received = client.get_ou_by_ou_id(ou_id=1)
    assert isinstance(received, OrgUnit)
