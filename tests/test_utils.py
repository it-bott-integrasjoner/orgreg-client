from orgreg_client import OrgUnit
from orgreg_client.utils import get_external_key


def test_get_external_key(ou_by_id):
    ext_key = "eph_id"
    ext_value = "eph-kurs"

    ext_key_value = get_external_key(OrgUnit(**ou_by_id), ext_key, ext_value)
    assert ext_key_value is not None
    assert ext_key_value.type == ext_key
    assert ext_key_value.source_system == ext_value
    assert ext_key_value.value == "104"
