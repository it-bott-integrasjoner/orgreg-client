from typing import Optional

from orgreg_client import OrgUnit
from orgreg_client.models import ExternalKey


def get_external_key(
    ou: OrgUnit, ext_key: str, key_value: str
) -> Optional[ExternalKey]:
    # Get external_key element when given org_unit, ext_key and key_value.

    ex_key_el = None
    if not ou.external_keys:
        return ex_key_el

    return next(
        (
            x
            for x in ou.external_keys
            if x.source_system == key_value and x.type == ext_key
        ),
        None,
    )
