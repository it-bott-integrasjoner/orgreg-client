"""Client for the OrgReg-api"""
from typing import Optional, Dict, Any, List, Union

import requests
from urllib.parse import quote, urljoin

from .models import ClientConfig, Endpoints, OrgUnit, OrgUnitList


class Client:
    """Client"""

    def __init__(self, config: ClientConfig):
        self.endpoints = Endpoints(config.endpoints)
        self.headers = config.headers or {}

    def get_ou(self) -> List[OrgUnit]:
        """Get dist lists"""
        url = str(self.endpoints.ou)
        result = requests.get(url, headers=self.headers)
        result.raise_for_status()
        result_json = result.json()

        return OrgUnitList.from_json(result_json).__root__

    def get_ou_by_ou_id(self, ou_id: int) -> OrgUnit:
        """Get org unit by ou_id"""
        url = f"{str(self.endpoints.ou)}{int(ou_id)}"

        result = requests.get(url, headers=self.headers)
        result.raise_for_status()
        return OrgUnit.from_dict(result.json())  # type: ignore

    def get_ou_by_sap_id(
        self, sap_id: int, populate: bool = True
    ) -> Union[List[int], List[OrgUnit]]:
        """
        "populate=True" returns the whole items, but "populate=False" would
        answer only with a orgreg-IDs.
        """
        return self.get_ou_by_ext_key("dfo_org_id", str(sap_id), populate)

    def get_ou_by_ext_key(
        self, ext_key: str, key_value: Optional[str] = None, populate: bool = True
    ) -> Union[List[int], List[OrgUnit]]:
        """Returns ou with given external_keys type and key_value, on populate=false
        only ou ids are returned"""
        url = urljoin(
            str(self.endpoints.ou_by_ext_type) % quote(ext_key, ""),
            quote(key_value, "") if key_value else None,
        )
        params = {}
        if populate:
            params["populate"] = "true"

        result = requests.get(url, params=params, headers=self.headers)
        result.raise_for_status()
        result_json = result.json()

        if not populate:
            return result_json["ous"]

        return OrgUnitList.from_json(result_json["ous"]).__root__


def get_client(**kwargs: Dict[str, Any]) -> Client:
    """
    Get a Client from configuration.
    """
    return Client(ClientConfig.from_dict(kwargs))  # type: ignore
