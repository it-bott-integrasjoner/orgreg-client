import datetime
import typing
from urllib.parse import urljoin

import json

import pydantic
from pydantic import Field


def _from_lower_camel(s: str) -> str:
    """Alias generator to avoid breaking PEP8"""
    x, *xs = s.split("_")
    return "".join([x.lower(), *map(str.capitalize, xs)])


class BaseModel(pydantic.BaseModel):
    @classmethod
    def from_dict(cls, data):  # type: ignore
        return cls(**data)  # noqa

    @classmethod
    def from_json(cls, json_data):  # type: ignore
        data = json.loads(json_data)
        return cls.from_dict(data)  # type: ignore

    def dict(  # type: ignore
        self, by_alias=True, exclude_unset=True, *args, **kwargs
    ) -> typing.Dict[str, typing.Any]:
        return super().dict(
            by_alias=by_alias, exclude_unset=exclude_unset, *args, **kwargs  # noqa
        )

    def keys(self):  # type: ignore
        return self.__fields__.keys()

    def __getitem__(self, item):  # type: ignore
        return self.__getattribute__(item)

    class Config:
        alias_generator = _from_lower_camel
        allow_population_by_field_name = True
        extra = pydantic.Extra.ignore


# CLIENT CLASSES


class EndpointsConfig(BaseModel):
    # Individual endpoints must be Optional. Set default value in __init__
    ou: typing.Optional[str]
    ou_by_sap_id: typing.Optional[str]
    base_url: pydantic.HttpUrl

    class Config:
        alias_generator = _from_lower_camel
        allow_population_by_field_name = True
        extra = pydantic.Extra.forbid


class Endpoints(BaseModel):
    ou: pydantic.HttpUrl
    ou_by_ext_type: pydantic.HttpUrl

    def __init__(self, config: EndpointsConfig) -> None:
        """Individual endpoints may be overridden"""
        # Defaults
        data: typing.Dict[str, str] = {
            "ou": "ou/",
            "ou_by_ext_type": "ou/search/%s/",
        }

        # Loop through all urls and create HttpUrl relative to config.base_url
        for k, _ in filter(
            lambda x: x[1].type_ == pydantic.HttpUrl, Endpoints.__fields__.items()
        ):
            if k in config.__fields__ and config[k]:
                data[k] = config[k]

            data[k] = urljoin(
                base=str(config.base_url), url=data[k], allow_fragments=False
            )

        super().__init__(**data)

    class Config:
        alias_generator = _from_lower_camel
        allow_population_by_field_name = True
        extra = pydantic.Extra.forbid


class ClientConfig(BaseModel):
    endpoints: EndpointsConfig
    headers: typing.Optional[typing.Dict[str, str]]

    class Config:
        alias_generator = _from_lower_camel
        allow_population_by_field_name = True
        extra = pydantic.Extra.forbid


# API CLASSES


class Address(BaseModel):
    street: typing.Optional[str]
    extended: typing.Optional[str]
    postal_code: typing.Optional[str]
    city: typing.Optional[str]
    state_or_province_name: typing.Optional[str]
    country: typing.Optional[str]


class Building(BaseModel):
    campus: typing.Optional[str]
    building: typing.Optional[str]
    room: typing.Optional[str]
    floor: typing.Optional[str]
    longitude: typing.Optional[str]
    latitude: typing.Optional[str]


class ExternalKey(BaseModel):
    type: str
    value: str
    source_system: typing.Optional[str]


class LocalisedValue(BaseModel):
    sme: typing.Optional[str]
    nno: typing.Optional[str]
    nob: typing.Optional[str]
    eng: typing.Optional[str]


class OrgUnit(BaseModel):
    ou_id: int
    valid_from: datetime.date
    start_date: typing.Optional[datetime.date]

    category: typing.Optional[typing.Dict[str, str]]
    valid_to: typing.Optional[datetime.date]
    end_date: typing.Optional[datetime.date]
    email: typing.Optional[str]
    note: typing.Optional[str]

    postal_address: typing.Optional[Address]
    visit_address: typing.Optional[Address]

    building: typing.Optional[Building]

    phone: typing.Optional[str]
    fax: typing.Optional[str]

    acronym: LocalisedValue = Field(default_factory=LocalisedValue)

    name: LocalisedValue = Field(default_factory=LocalisedValue)

    homepage: LocalisedValue = Field(default_factory=LocalisedValue)

    short_name: LocalisedValue = Field(default_factory=LocalisedValue)

    long_name: LocalisedValue = Field(default_factory=LocalisedValue)

    external_keys: typing.List[ExternalKey] = Field(default_factory=list)
    tags: typing.List[str] = Field(default_factory=list)

    parent: typing.Optional[int]
    children: typing.List[int] = Field(default_factory=list)

    # Historic data
    predecessors: typing.List[int] = Field(default_factory=list)


_OuListT = typing.TypeVar("_OuListT", bound="OrgUnitList")


class OrgUnitList(BaseModel):
    __root__: typing.List[OrgUnit]

    @classmethod
    def from_json(
        cls: typing.Type[_OuListT], data: typing.List[typing.Dict[str, typing.Any]]
    ) -> _OuListT:
        return cls(__root__=pydantic.parse_obj_as(typing.List[OrgUnit], data))  # noqa
