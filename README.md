# orgreg-client

[![pipeline status](https://git.app.uib.no/it-bott-integrasjoner/orgreg-client/badges/master/pipeline.svg)](https://git.app.uib.no/it-bott-integrasjoner/orgreg-client/-/commits/master)
[![coverage report](https://git.app.uib.no/it-bott-integrasjoner/orgreg-client/badges/master/coverage.svg)](https://git.app.uib.no/it-bott-integrasjoner/orgreg-client/-/commits/master)

Client for the OrgReg-api version 3


## Set up development environment

### Virtual environment

#### Create

```bash
virtualenv --python=python3 venv
```

#### Activate

```bash
source venv/bin/activate
```

### Install dependencies

```bash
pip install -r requirements.txt -r requirements-dev.txt -r requirements-test.txt
```

This should install all dependencies, including development dependencies (code formatter, linters etc).

## Running the tests

### Run all tests

Use [tox](https://tox.readthedocs.io/) to run all tests and linters.

```bash
tox
```

### Run pytest

```bash
python -m pytest
```

### Type check

We use static type checker [mypy](http://mypy-lang.org/).

```bash
python -m mypy -p orgreg_client
```


## Example

```python
from orgreg_client.client import get_client

config_dict = {
    "endpoints": {
        "baseUrl": "https://example.com/orgreg/v3/"
    },
    "headers": {
        "X-Gravitee-Api-Key": "..."
    },
}

client = get_client(**config_dict)
data = client.get_ou()
print(data)
```

You may override individual endpoints like so:

```json
{
    "endpoints": {
        "baseUrl": "https://example.com/orgreg/v3/",
        "ou": "path/relative/to/baseUrls",
        "another": "https://new.example.com/orgreg/v3/another"
    }
}
```
